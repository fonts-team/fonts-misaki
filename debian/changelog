fonts-misaki (20210505-1) unstable; urgency=medium

  * New upstream version 20210505
  * Trim trailing whitespace.
  * debian/control
    - Set Standards-Version: 4.5.1
  * debian/copyright
    - Add Upstream-Contact for twitter account and update copyright year

 -- Hideki Yamane <henrich@debian.org>  Sun, 16 May 2021 13:44:27 +0900

fonts-misaki (20191019-2) unstable; urgency=medium

  * debian/control
    - set debhelper-compat (= 13)
    - set Standards-Version: 4.5.0
    - drop dependencies for old ttf-misaki package
  * update debian/salsa-ci.yml, disable unnecessary tests
  * debian/{control,copyright,watch}
    - update upstream site to https

 -- Hideki Yamane <henrich@debian.org>  Sun, 03 May 2020 01:40:51 +0900

fonts-misaki (20191019-1) unstable; urgency=medium

  * New upstream version 20191019
  * debian/control
    - drop Build-Depends: debhelper
    - set Standards-Version: 4.4.1

 -- Hideki Yamane <henrich@debian.org>  Thu, 24 Oct 2019 21:00:54 +0900

fonts-misaki (20190603a-1) unstable; urgency=medium

  * New upstream version 20190603a
  * debian/rules
    - drop override_dh_auto_build, fixed in upstream, unnecessary anymore
  * debian/control
    - drop some description

 -- Hideki Yamane <henrich@debian.org>  Sun, 18 Aug 2019 22:23:53 +0900

fonts-misaki (20190603-1) unstable; urgency=medium

  * New upstream version 20190603
    - [new font]:misaki_gothic_2nd.ttf
  * debian/control
    - set Standards-Version: 4.4.0
    - add Rules-Requires-Root: no
  * add debian/salsa-ci.yml to exec CI on salsa.debian.org

 -- Hideki Yamane <henrich@debian.org>  Mon, 12 Aug 2019 14:53:34 +0900

fonts-misaki (20190203a-1) unstable; urgency=medium

  * New upstream version 20190203a
  * debian/copyright
    - update new upstream url
    - use https
    - update copyright year
  * debian/control
    - set Standards-Version: 4.3.0
    - use debhelper-compat and drop debian/compat file
    - update Vcs-*
    - update Homepage
  * debian/rules
    - use default compression
    - remove executable bit for *.ttf
  * debian/watch
    - update url and fix pattern
  * debian/fonts-misaki.preinst
    - drop it, it's unnecessary anymore

 -- Hideki Yamane <henrich@debian.org>  Sat, 09 Feb 2019 00:14:45 +0900

fonts-misaki (20150410-3) unstable; urgency=medium

  * debian/control
    - use https for Vcs-*
    - set "Build-Depends: debhelper (>= 10)"
    - set Standards-Version: 4.0.0
  * debian/compat
    - set 10
  * debian/watch
    - update to version 4

 -- Hideki Yamane <henrich@debian.org>  Thu, 20 Jul 2017 05:49:47 +0900

fonts-misaki (20150410-2) unstable; urgency=medium

  * fix regression in debian/fonts-misaki.preinst

 -- Hideki Yamane <henrich@debian.org>  Sun, 03 Jan 2016 16:31:33 +0900

fonts-misaki (20150410-1) unstable; urgency=medium

  * Imported Upstream version 20150410
    - fix "zero font height" issue (Closes: #790417)
  * debian/watch
    - update to deal with upstream's change
  * debian/control
    - set Standards-Version: 3.9.6
    - update Vcs-Browser
  * debian/copyright
    - convert to copyright format 1.0
  * update debian/fonts-misaki.docs
  * debian/fonts-misaki.preinst
    - use pathfind to fix lintian "command-with-path-in-maintainer-script"
      warning

 -- Hideki Yamane <henrich@debian.org>  Sat, 04 Jul 2015 01:26:58 +0900

fonts-misaki (11-20080603-15) unstable; urgency=low

  * debian/control
    - drop ttf- package
    - use canonical URL for Vcs-*

 -- Hideki Yamane <henrich@debian.org>  Sun, 16 Jun 2013 05:32:52 +0900

fonts-misaki (11-20080603-14) unstable; urgency=low

  * New upstream release
  * debian/control
    - set Multi-Arch: foreign
    - move from svn to git
    - set Standards-Version: 3.9.4

 -- Hideki Yamane <henrich@debian.org>  Tue, 04 Jun 2013 20:47:17 +0900

fonts-misaki (11-20080603-13) unstable; urgency=low

  * debian/rules
    - compress with xz
  * debian/control
    - add "Pre-Depends: dpkg (>= 1.15.6~)"
    - bump up debhelper version to deal with xz option
    - replace s/Conflicts/Breaks/ to more Policy polite.
  * debian/compat
    - set 9
  * debian/fonts-misaki.install,lintian*
    - remove un-used overrides

 -- Hideki Yamane <henrich@debian.org>  Wed, 13 Jun 2012 03:34:02 +0900

fonts-misaki (11-20080603-12) unstable; urgency=low

  * debian/control
    - set "Standards-Version: 3.9.3"
    - add "Priority: extra" to transitional package

 -- Hideki Yamane <henrich@debian.org>  Sat, 10 Mar 2012 05:48:24 +0900

fonts-misaki (11-20080603-11) unstable; urgency=low

  * debian/fonts-misaki.install
    - revert change for font install path

 -- Hideki Yamane <henrich@debian.org>  Tue, 27 Sep 2011 23:23:58 +0900

fonts-misaki (11-20080603-10) unstable; urgency=low

  * debian/control
    - introduce ttf-misaki as transitional dummy package
    - specify version for Conflicts: and Replaces:
    - drop Provides:
    - fix Vcs-* field

 -- Hideki Yamane <henrich@debian.org>  Tue, 16 Aug 2011 01:50:16 +0900

fonts-misaki (11-20080603-9) unstable; urgency=low

  [Christian Perrier]
  * Drop x-ttcidfont-conf, fontconfig et al. from Suggests

  [Hideki Yamane]
  * rename from ttf-misaki to fonts-misaki
  * debian/control
    - remove DM stuff
    - set "Build-Depends: debhelper (>= 7.0.50~)
    - set Standards-Version: 3.9.2
  * remove unnecessary backup file
  * add lintian overrides to ignore warnings during transition

 -- Hideki Yamane <henrich@debian.org>  Tue, 21 Jun 2011 05:13:29 +0900

ttf-misaki (11-20080603-8) unstable; urgency=low

  * Bump up Standards-Version: 3.8.3
  * use debhelper7
  * switch source format 3.0 (quilt)
  * drop defoma

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Sat, 12 Dec 2009 18:11:48 +0900

ttf-misaki (11-20080603-7) unstable; urgency=low

  * debian/control
    - fix typo, set "Suggests: fontconfig (>= 2.6.0-4)"

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Thu, 25 Jun 2009 17:41:38 +0900

ttf-misaki (11-20080603-6) unstable; urgency=low

  * debian/{prerm,postinst}
    - wrongly removed them, so add them again.
  * debian/rules
    - add missing dh_installdefoma

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Wed, 24 Jun 2009 22:53:07 +0900

ttf-misaki (11-20080603-5) unstable; urgency=low

  * debian/control:
    - Bump up Standards-Version: 3.8.2
    - "Suggests: fontconfig (>= 2.6.0-4)
  * debian/{prerm,postinst}
    - remove fc-cache call

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Sun, 21 Jun 2009 23:00:20 +0900

ttf-misaki (11-20080603-4) unstable; urgency=low

  * debian/watch:
    - DEHS is great, thanks to Raphael Geissert <atomo64+debian@gmail.com>

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Sun, 05 Apr 2009 20:46:16 +0900

ttf-misaki (11-20080603-3) unstable; urgency=low

  * debian/control:
    - new section "fonts"

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Wed, 25 Mar 2009 07:17:15 +0900

ttf-misaki (11-20080603-2) unstable; urgency=low

  * debian/watch: fix broken watch file.
  * debian/control: Bump up Standards-Version: 3.8.1

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Sat, 14 Mar 2009 16:46:53 +0900

ttf-misaki (11-20080603-1) unstable; urgency=low

  * Initial release. (Closes: #518717)

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Sun, 08 Mar 2009 18:58:38 +0900
